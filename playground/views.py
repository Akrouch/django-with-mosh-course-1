from django.shortcuts import render
from django.db import transaction
from store.models import Collection


@transaction.atomic()
def say_hello(request):
    with transaction.atomic():
        collection = Collection(pk=1)
        collection.featured_product_id = 1
        collection.title = 'Pijas Collection'
        collection.save()
        
    return render(request, 'hello.html', {'name': 'Mosh', 'coll Name': collection.title})

