from django.contrib import admin, messages
from django.db.models.aggregates import Count
from django.utils.html import format_html, urlencode
from django.urls import reverse
from django.db.models.query import QuerySet

from . import models
# Register your models here.





@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'membership', 'orders']
    list_editable = ['membership']
    list_per_page = 10
    
    ordering = ['first_name', 'last_name']
    search_fields = ['first_name__istartswith', 'last_name__istartswith']
    
    @admin.display(ordering='orders')
    def orders(self, customer: models.Customer):
        url = (
            reverse('admin:store_order_changelist')
            + '?'
            + urlencode({
                'customer__id': str(customer.id)
            })
        )
        return format_html('<a href={}>{}</a>', url, customer.orders)
    
    def get_queryset(self, request):
        return super().get_queryset(request).annotate(
            orders=Count('order')
        )
    
    
class InventoryFilter(admin.SimpleListFilter):
    title = 'inventory' # name that comes after "by"
    parameter_name = 'inventory' # appears in url
    
    
    low_inventory = '<10' # appears in the url and needs to match on both methods below
    high_inventory = '>80' # appears in the url and needs to match on both methods below
    
    def lookups(self, request, model_admin):
        return [
            (self.low_inventory, 'LOW (<10)'), 
            (self.high_inventory, 'HIGH (>80)')
        ]
    
    def queryset(self, request, queryset: QuerySet):
        if self.value() == self.low_inventory:
            return queryset.filter(inventory__lt=10)
        elif self.value() == self.high_inventory:
            return queryset.filter(inventory__gt=80)




#convention
@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    actions = ['clear_inventory']
    autocomplete_fields = ['collection']
    list_display = ['title', 'unit_price',  'collection_title', 'inventory']
    list_editable = ['unit_price']
    list_filter = ['collection', 'last_update', InventoryFilter]
    list_per_page = 10
    search_fields = ['title__startswith']
    ordering = ['title']
    list_select_related = ['collection']
    
    def collection_title(self, product: models.Product):
        return product.collection.title
    
    @admin.action(description='Clear inventory')
    def clear_inventory(self, request, queryset: QuerySet):
        updated_count = queryset.update(inventory=0)
        self.message_user(
            request,
            f'{updated_count} products were successfully updated to inventory = 0',
            # messages.SUCCESS # optional
        )
    
            
    
#or


@admin.register(models.Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ['title', 'products_count']
    search_fields = ['title']
    @admin.display(ordering='products_count')
    def products_count(self, collection: models.Collection):
        url = (
            reverse('admin:store_product_changelist')
            + '?'
            + urlencode({
                'collection__id': str(collection.id)
            })
        )
        
        url = f'{reverse("admin:store_product_changelist")}?collection__id={collection.id}'
        return format_html('<a href={}>{}</a>', url, collection.products_count)
    
    def get_queryset(self, request):
        return super().get_queryset(request).annotate(
            products_count=Count('product')
        )


class OrderItemInline(admin.StackedInline):
    autocomplete_fields = ['product']
    model = models.OrderItem
    min_num = 1
    max_num = 10
    extra = 0

@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'placed_at', 'customer_full_name']
    list_select_related = ['customer']
    autocomplete_fields = ['customer']
    inlines= [OrderItemInline]
    
    
    
    @admin.display(ordering='customer')
    def customer_full_name(self, order: models.Order):
        return f'{order.customer.first_name} {order.customer.last_name}'
